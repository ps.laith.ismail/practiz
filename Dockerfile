FROM openjdk:8u265-slim

WORKDIR /apps

COPY cscripts.sh /apps
COPY artifacts/*.jar /apps

#RUN adduser user && mkdir /apps && chown user:user /apps

EXPOSE 8080
#USER user

ENTRYPOINT sh cscripts.sh

